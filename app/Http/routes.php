<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// ===================================
// STATIC PAGES ======================
// ===================================


// show a static view for your home page (app/resources/views/home.blade.php)


// about page (app/resources/views/skills.blade.php)
//Route::resource('create', 'CreateController');

//
//Route::get('/home', 'HomeController@index');
Route::group(['middleware' => ['web']], function() {

    Route::auth();
    Route::resource('/', 'QuestionnaireController@index');
    Route::resource('/questionnaires', 'QuestionnaireController');
    Route::resource('/questions', 'QuestionController');
    Route::resource('/questions/create', 'QuestionController@create');
    Route::resource('/answers', 'AnswerController');
    Route::resource('/answers/create', 'AnswerController@create');
    Route::resource('/take', 'ResponseController');
});
