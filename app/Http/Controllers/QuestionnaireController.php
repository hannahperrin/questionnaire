<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Questionnaire;
use Auth;
use App\Http\Requests;

class QuestionnaireController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    displays on (/questionnaires)
    public function index()
    {
//        gets all questionnaires
        $user = Auth::user();
        $questionnaires = Questionnaire::where('user_id', $user->id)->get();
//        returns the view with the questionnaires
        return view ('admin.questionnaires.index', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view ('admin.questionnaires.create', ['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Questionnaire::create($request->all());
        return redirect('/questionnaires');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questionnaire=Questionnaire::findOrFail($id);
        return view ('admin.questionnaires.show', ['questionnaire' => $questionnaire]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questionnaire=Questionnaire::findOrFail($id);
        return view ('admin.questionnaires.edit', ['questionnaire' => $questionnaire]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $questionnaire=Questionnaire::findOrFail($id);
        $questionnaire->update($request->all());
        return redirect('/questionnaires');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionnaire = Questionnaire::findOrFail($id);
        $questionnaire->delete();
        return redirect('/questionnaires');
    }
}
