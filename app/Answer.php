<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'title', 'question_id',
    ];

    public function question() {
        return $this->belongTo('App\Question');
    }

    public function response() {
        return $this->hasMany('App\Response');
    }
}
