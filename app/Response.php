<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $fillable = [
        'title', 'answer_id',
    ];

    public function answer() {
        return $this->belongTo('App\Answer');
    }

}
