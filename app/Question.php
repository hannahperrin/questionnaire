<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'title', 'questionnaire_id',
    ];

    public function questionnaire() {
        return $this->belongTo('App\Questionnaire');
    }

    public function answer() {
        return $this->hasMany('App\Answer');
    }
}
