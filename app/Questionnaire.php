<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $fillable = [
        'title', 'user_id',
    ];

    public function user() {
        return $this->belongTo('App\User');
    }

    public function question() {
        return $this->hasMany('App\Question');
    }
}
