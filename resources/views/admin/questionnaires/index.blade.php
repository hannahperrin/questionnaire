@extends('layouts.master')

@section('title', 'Questionnaires')

@section('content')

    <h1>All Questionnaires</h1>
{{--checks the variable has a value--}}
    @if(isset($questionnaires))
        {{--loops over each questionnaire--}}
        @foreach($questionnaires as $questionnaire)
            {{--prints the questionnaire id--}}
            <h3><a href="/questionnaires/{{ $questionnaire->id }}">{{ $questionnaire->title }}</a></h3>
            <h6><a href="/questionnaires/{{ $questionnaire->id }}/edit/">Edit Questionnaire Title</a></h6>
            {!! Form::open(['method' => 'DELETE', 'route' => ['questionnaires.destroy', $questionnaire->id]]) !!}
                {!! Form::button('Delete Questionnaire', ['type' => 'submit', 'class' => 'button expand alert', 'title' => 'Delete Questionnaire']) !!}
            {!! Form::close() !!}
            {{--@foreach($questionnaire->questions as $questions)--}}

            {{--@endforeach--}}
        @endforeach
    @endif

@endsection