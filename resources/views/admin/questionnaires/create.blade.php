@extends('layouts.master')

@section('title', 'Questionnaires')

@section('content')

    <h3>Title of Questionnaire:</h3>

    {!! Form::open(['url' => 'questionnaires']) !!}

    <div class="form-group">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    @if(isset($user))
        {!! Form::hidden('user_id', $user->id) !!}
    @endif
    {!! Form::submit('Submit', null, ['class' => 'form-control']) !!}

    {!! Form::close() !!}



@endsection