@extends('layouts.master')

@section('title', 'Questionnaires')

@section('content')

    {{--checks the variable has a value--}}
    @if(isset($questionnaire))

        <h1>{{ $questionnaire->title }}</h1>
        <h4><a href="/questions/create/{{ $questionnaire->id }}">Add a Question</a></h4>

        @foreach($questionnaire->question as $question)
            <h3>{{ $question->title }}</h3>
            <h6><a href="/questions/{{ $question->id }}/edit/">Edit Question Title</a></h6>
            <h6><a href="/questions/{{ $question->id }}">View Answers</a></h6>
            {!! Form::open(['method' => 'DELETE', 'route' => ['questions.destroy', $question->id]]) !!}
            {!! Form::button('Delete Question', ['type' => 'Delete Question', 'class' => 'button expand alert', 'title' => 'Delete Questions']) !!}
            {!! Form::close() !!}

        @endforeach

    @endif

@endsection