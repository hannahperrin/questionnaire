@extends('layouts.master')

@section('title', 'Questionnaires')

@section('content')

    <h3>Edit Questionnaire Title:</h3>

    @if(isset($questionnaire))

        {!! Form::model($questionnaire, ['method' => 'PATCH', 'url' => 'questionnaires/' . $questionnaire->id]) !!}

        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', $questionnaire->title, ['class' => 'form-control']) !!}
        </div>


        {!! Form::hidden('user_id', $questionnaire->user_id) !!}

        {!! Form::submit('Submit', null, ['class' => 'form-control']) !!}

        {!! Form::close() !!}
    @endif



@endsection