@extends('layouts.master')

@section('title', 'Add a Question')

@section('content')

    <h3>Add a Question</h3>

    {!! Form::open(['url' => 'questions']) !!}

    <div class="form-group">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    @if(isset($questionnaire))
        {!! Form::hidden('questionnaire_id', $questionnaire->id) !!}
    @endif
    {!! Form::submit('Submit', null, ['class' => 'form-control']) !!}

    {!! Form::close() !!}



@endsection