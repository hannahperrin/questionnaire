@extends('layouts.master')

@section('title', 'Questions')

@section('content')

    {{--checks the variable has a value--}}
    @if(isset($question))

        <h1>{{ $question->title }}</h1>

        <h4><a href="/answers/create/{{ $question->id }}">Add an Answer</a></h4>

        @foreach($question->answer as $answer)
            <h3>{{ $answer->title }}</h3>
            <h6><a href="/answers/{{ $answer->id }}/edit/">Edit Answer</a></h6>
            {!! Form::open(['method' => 'DELETE', 'route' => ['answers.destroy', $answer->id]]) !!}
            {!! Form::button('Delete', ['type' => 'Delete Answer', 'class' => 'button expand alert', 'title' => 'Delete Answer']) !!}
            {!! Form::close() !!}

        @endforeach



    @endif


@endsection