@extends('layouts.master')

@section('title', 'Question')

@section('content')

    <h3>Edit Question Title:</h3>

    @if(isset($question))

        {!! Form::model($question, ['method' => 'PATCH', 'url' => 'questions/' . $question->id]) !!}

        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', $question->title, ['class' => 'form-control']) !!}
        </div>


        {!! Form::hidden('question_id', $question->questionnaire_id) !!}

        {!! Form::submit('Submit', null, ['class' => 'form-control']) !!}

        {!! Form::close() !!}
    @endif



@endsection