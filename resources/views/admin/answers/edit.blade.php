@extends('layouts.master')

@section('title', 'Answers')

@section('content')

    <h3>Edit Answer:</h3>

    @if(isset($answer))

        {!! Form::model($answer, ['method' => 'PATCH', 'url' => 'answers/' . $answer->id]) !!}

        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', $answer->title, ['class' => 'form-control']) !!}
        </div>


        {!! Form::hidden('question_id', $answer->user_id) !!}

        {!! Form::submit('Submit', null, ['class' => 'form-control']) !!}

        {!! Form::close() !!}
    @endif



@endsection