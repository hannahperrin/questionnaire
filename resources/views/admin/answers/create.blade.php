@extends('layouts.master')

@section('title', 'Add an answer')

@section('content')

    <h3>Add an Answer</h3>

    {!! Form::open(['url' => 'answers']) !!}

    <div class="form-group">
        {!! Form::label('title', 'Title:') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>

    @if(isset($question))
        {!! Form::hidden('question_id', $question->id) !!}
    @endif
    {!! Form::submit('Submit', null, ['class' => 'form-control']) !!}

    {!! Form::close() !!}



@endsection