<nav class="navbar navbar-inverse navbar-fixed-top">

    <div class="container-fluid">

        <ul class="nav navbar-nav">

            <li><a href="/questionnaires">View Questionnaires</a></li>

            <li><a href="/questionnaires/create">Create Questionnaire</a></li>

        </ul>
    </div>
</nav>
